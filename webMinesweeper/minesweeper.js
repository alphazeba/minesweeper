/*
Minesweeper
Aron Hommas
2017
This was written to practice javascript, flood fill, and dynamically resizing displays.
*/

  ///////////
 //classes//
///////////



class TextBox{
	constructor(x,y,w,h,text,font,color){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.text = text;
		this.font = font;
		this.color = color;
		this.size = 0;
		this.calculateFontSize();
	}
	
	setText(text){
		this.text = text;
		this.calculateFontSize();
	}
	
	setColor(color){
		this.color = color;
	}
	
	draw(context){
		context.fillStyle = this.color;
		context.font = this.size.toString() + "px " + this.font;
		context.fillText(this.text,  this.x+this.w/2-this.size/2*this.text.length/2,  this.y+this.h/2+this.size/3);
	}
	
	calculateFontSize(){
		//resizes the font to fit within the textbox.
		this.size = Math.floor(this.w/this.text.length);
		if(this.size > this.h){
			this.size = this.h;
		}
	}
	
	resize(x,y,w,h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.calculateFontSize();
	}
}

class Timer{
	constructor(){
		this.time = 0;
		this.x= 0;
		this.y = 0;
		this.w = 0;
		this.h = 0;
		this.text = new TextBox(0,0,0,0,"0:00","Arials","white");
		this.running = true;
	}
	
	start(){
		this.running = true;
	}
	
	stop(){
		this.running = false;
	}
	
	getTime(){
		return time;
	}
	
	getTimeString(){
		var s = Math.floor(this.time/60).toString() + ":";
		
		var seconds = this.time%60;
		
		if(seconds < 10){
			s+= "0";
		}
		s+= seconds.toString();
		
		return s;
	}
	
	draw(context){
		this.text.draw(context);
	}
	
	onSecond(){
		if(this.running){
			this.time++;
		}
		this.text.setText(this.getTimeString());
	}
	
	onResize(x,y,w,h){
		this.x = x;
		this.y = y;
		this.w=w;
		this.h=h;
		this.text.resize(x,y,w,h);
	}
}

class Button{
	constructor(x,y,w,h,text){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.text = new TextBox(x,y,w,h,text,"Arials","black");
		
		this.clicked =false;
	}
	
	draw(context){
		//draw background
		context.fillStyle = "gray";
		context.fillRect(this.x,this.y,this.w*0.98,this.h*0.98);
		
		//draw text
		this.text.draw(context);
	}
	
	onClick(x,y){
		//checks click was inside the box.
		if(x>this.x && x < this.x+this.w && y > this.y && y < this.y+this.h){
			this.clicked = true;
			return true;
		}
		return false;
	}
	
	wasClicked(){
		//returns whether or not this button has been clicked since being created.
		return this.clicked;
	}

	resize(x,y,w,h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.text.resize(x,y,w,h);
	}
}

class Menu{
	constructor(){
		this.buttons = [];
		this.buttons.push(new Button(0,0,0,0,"Easy"));
		this.buttons.push(new Button(0,0,0,0,"Medium"));
		this.buttons.push(new Button(0,0,0,0,"Hard"));
		this.buttons.push(new Button(0,0,0,0,"Insane"));
		this.buttons.push(new Button(0,0,0,0,"Zen"));
		this.selection = -1;
	}
	
	draw(context){
		//draws all of the buttons.
		for(var i = 0 ;i < 5 ; i++){
			this.buttons[i].draw(context);
		}
	}
	
	onResize(w,h){
		var height = h/11;
		var width = w/5*3;
		var x = w/5;
		
		for(var i = 0 ; i < 5 ; i++){
			this.buttons[i].resize(x,height*2*i+height,width,height);
		}
	}
	
	onClick(x,y){
		//sends the click to all of its buttons.
		for(var i = 0 ; i < 5; i++){
			if(this.buttons[i].onClick(x,y)){
				this.selection = i;
			}
		}
	}
	
	wasASelectionMade(){
		//returns whether or not any button has been clicked.
		return this.selection != -1;
	}
	
	getSelection(){
		//returns the specific selection.
		return this.selection;
	}
}

class Box{
	
	constructor( rows,  cols,  width,  height ,  px ,  py,anchorX,anchorY){
		
		this.width = width/cols;
		this.height = height/rows;
		//px and py define which position on the grid this particular box is.
		this.px = px;
		this.py = py;
		this.x = this.width*px + anchorX;
		this.y = this.height*py + anchorY;
		//bffr is the size of the space between boxes.
		this.bffr = 3;
		
		//each box can be in one of 3 states.
		//blank the block is covered
		//marked the block is covered and marked.
		//sweeped the block has been uncovered.
		this.state = "blank";
		this.num = Math.floor(Math.random() * 6);
		
		this.bomb = false;
	}
	
	resize(width,height,anchorX,anchorY){
		this.width = width;
		this.height = height;
		this.x = this.width*this.px + anchorX;
		this.y = this.height*this.py + anchorY;
	}
	
	draw(context){
		//draw background.
		context.fillStyle="gray";
		context.fillRect(this.x,this.y,this.width-this.bffr,this.height-this.bffr);
		
		//draw state specific things
		if(this.state === "blank"){
			//doesn't need anything in addition to the background.
		}
		else if(this.state === "marked"){
			//draw a red mark
			context.fillStyle = "red";
			context.fillRect(this.x + (this.width-this.bffr)/4, this.y + (this.height-this.bffr)/4,(this.width-this.bffr)*2/4,(this.height-this.bffr)*2/4);
		}
		else if(this.state === "sweeped"){
			//if the box has a bomb, draw an explosion, otherwise draw the number.
			if(this.isBomb()){
				//draw a boom.
				context.fillStyle = "red";
				context.beginPath();
				context.arc(this.x + this.width/2, this.y+ this.height/2, (this.width+this.height)/2 *11/10,0,2 * Math.PI);
				context.fill();
			}
			else{
				//draw a slightly smaller black square
				var w = this.width-this.bffr;
				var h = this.height-this.bffr;
				
				context.fillStyle = "black";
				context.fillRect(this.x + w/40,this.y + h/40 , w/40*38 , h/40*38);
				
				//draw the number if not a zero.
				//switch to set the colors of the different numbers.
				switch(this.num){
				case 0:
				context.fillStyle = "black";
				break;
				
				case 1:
				context.fillStyle = "#54f9ff";
				break;
				
				case 2:
				context.fillStyle = "#29ff1e";
				break;
				
				case 3:
				context.fillStyle = "#ffbb1e";
				break;
				
				case 4:
				context.fillStyle = "#ff471e";
				break;
				
				case 5:
				context.fillStyle = "#ff0505";
				break;
				
				case 6:
				context.fillStyle = "#ff056d";
				break;
				
				case 7:
				context.fillStyle = "#c805ff";
				break;
				
				case 8:
				context.fillStyle = "#c2a9e8"
				break;
			}
				context.font = Math.floor(this.height/4*3).toString() + "px Arials";
				context.fillText(this.num.toString(),this.x+this.width*0.3,this.y+this.height*0.8);
			}
		}
	}
	
	getState(){
		//returns blank, marked, or sweeped.
		return this.state;
	}
	
	setState(s){
		this.state = s;
	}
	
	setBomb(){
		//makes this box a bomb
		//returns false if the box was already a bomb
		if(this.isBomb()){
			return false;
		}
		this.bomb = true;
		return true;
	}
	
	isBomb(){//returns whether or not this is a bomb.
		return this.bomb;
	}
	
	blewup(){
		//if the box is a bomb and it was sweeped, then the box is blownup.
		return this.isBomb() && this.isSweeped();
	}
	
	isSweeped(){
		return this.getState() == "sweeped";
	}
	
	isMarked(){
		return this.getState() === "marked";
	}
	
	sweep(){
		this.setState("sweeped");
	}
	
	getNum(){
		//gets the number of surrounding bombs.
		return this.num;
	}
	
	setNum(n){
		this.num = n;
	}
	
	wasClicked(dblclick){
		//this is sent to the box to inform the box it has been clicked.
		//board calculates which box was clicked.
		
		//TODO
		//do clicky things.
		if(dblclick){
			this.sweep();
		}
		else{
			switch(this.state){
			case "blank":
				this.setState("marked");
				break;
			
			case "marked":
				this.setState("blank");
				break;
			}
		}
		return true;
	}
}

class Board{
	constructor(x,y,width,height,rows,cols,numBombs){
		//vars
		this.rows = rows;
		this.cols = cols;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.gameOver = false;
		this.victory = false;
		this.marks = 0;
		this.bombs = numBombs;
		
		
		
		//intialization
		this.boxes = [];
		//define the box array;
		for(var j=0;j < this.rows; j++){
			for(var i = 0 ; i < this.cols; i++){
				//defines all of the boxes in a single array.
				this.boxes.push(new Box(this.rows,this.cols,width,height,i,j,x,y));
			}
		}
		
		//TODO 
		//make some of the boxes have bombs.
		for(var i = 0 ; i < numBombs ; i++){
			do{
				var successfullyPlacedBomb = this.boxes[Math.floor(Math.random()*this.rows*this.cols)].setBomb();
			}while(!successfullyPlacedBomb);
		}
		
		//set the numbers of the boxes.
		this.calculateBoard();
	}
	
	calculateBoard(){
		//sets the number values of all the boxes.
		for(var i = 0 ; i < this.cols*this.rows ; i++){
			this.boxes[i].setNum(this.countNeighborBombs((i % this.cols),Math.floor(i/this.cols)));
		}
	}
	
	countNeighborBombs(x,y){
		var output = 0;
		for(var ix = -1; ix < 2 ; ix++){
			for(var iy = -1 ; iy < 2 ; iy++){
				if(ix != 0 || iy != 0){
					var curx = x+ix;
					var cury = y+iy;
					if(curx >= 0 && curx < this.cols && cury >= 0 && cury < this.rows){
						if(this.boxes[curx + (cury*this.cols)].isBomb()){
							output++;
						}
					}
				}
			}
		}
		return output;
	}
	
	resize(x,y,width,height){
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		var w = width/this.cols;
		var h = height/this.rows;
		for(var i = 0 ; i< this.rows*this.cols ; i++){
			this.boxes[i].resize(w,h,x,y);
		}
	}
	
	draw(context){
		var exploded = -1;
		for(var i=0;i < this.rows*this.cols ; i++){
			//check if a box exploded.
			if(this.boxes[i].blewup()){
				//stores the exploded box so that it can be redrawn later.
				exploded = i;
			}
			else{
				this.boxes[i].draw(context);
			}
		}
		
		//draw the exploded box last so that it shows up on top.
		if(exploded > -1){
			this.boxes[exploded].draw(context);
		}
	}
	
	onClick(x,y,dblclick){
		if(this.gameOver){//the click is ignored if the game is over.
			return;
		}
		
		//find which box was clicked and stores col and row in dx and dy.
		var dx = Math.floor((x-this.x)/(this.width/this.cols));
		var dy = Math.floor((y-this.y)/(this.height/this.rows));
		var curbox = dx + dy*this.cols;
		
		if(dx < 0 || dx >= this.cols || dy < 0 || dy >= this.rows){//if invalid input
			return;
		}
		
		var wasUnsweeped = !this.boxes[curbox].isSweeped();
		var wasMarked = this.boxes[curbox].isMarked();
		
		this.boxes[curbox].wasClicked(dblclick);
		
		if(wasMarked != this.boxes[curbox].isMarked()){
			//marking was added or removed.
			if(wasMarked){
				//marking was removed.
				this.marks--;
			}
			else{
				//marking was added.
				this.marks++;
			}
		}
		
		if(dblclick){
			if(this.boxes[curbox].isBomb()){
				this.gameOver = true;
			}
			if(wasUnsweeped && this.boxes[curbox].getNum()===0){
				//begins clearing neighbor boxes if the uncovered square has a 0 neighbor bombs around it.
				this.clear(dx,dy);
			}
		}
		
		
	}
	
	clear(x,y){
		//clear uses a recursive flood fill algorithm to check all neighbor squares and continue clearing.
		for(var ix = -1; ix < 2 ; ix++){
			for(var iy = -1; iy < 2 ; iy++){
				if(ix != 0 || iy != 0){
					var curx = x+ix;
					var cury = y+iy;
					if(curx >= 0 && curx < this.cols && cury >=0 && cury < this.rows){
						//clear the box
						var curbox = curx + (cury*this.cols);
						if(!this.boxes[curbox].isSweeped() && !this.boxes[curbox].isMarked()){
							this.boxes[curbox].sweep();
							
							if(this.boxes[curbox].getNum() === 0){
								this.clear(curx,cury);
							}
						}
					}
				}
			}
		}
	}

	isGameOver(){
		return this.gameOver;
	}

	getIfVictory(){
		if(!this.isGameOver()){
			return false;
		}
		return this.victory;
	}
	
	getBombsRemaining(){
		var remaining = this.bombs - this.marks;
		
		//if all bombs are correctly marked game over victory state is entered.
		if(remaining <= 0){//can sometimes temporarily fall below zero
			if(this.checkIfAllBombsAreCorrectlyMarked()){
				this.gameOver = true;
				this.victory = true;
			}
		}
		
		return remaining;
	}
	
	checkIfAllBombsAreCorrectlyMarked(){
		//returns false if there are any mistakes made.
		//otherwise returns true;
		
		for(var i = 0 ;i < this.rows*this.cols ; i++){
			if(this.boxes[i].isBomb() && !this.boxes[i].isMarked()){ //if it is a bomb and not marked
				return false;
			}
			if(!this.boxes[i].isBomb() && this.boxes[i].isMarked()){ //if this is not a bomb and it is marked.
				return false;
			}
		}
		return true;
	}
}

class UI{
	//should show bombs remaining
	//the restart button
	//and time maybe?
	constructor(x,y,width,height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.bombsRemaining = 0;
		
		this.scoreBox = new TextBox(0,0,0,0,this.bombsRemaining.toString(),"Arials","white");
		this.textBox = new TextBox(0,0,0,0,"Clear the mines!","Arials","white");
		
		
		this.state = "game";
		
		this.obscured = false;
		
		this.restartButton = new Button(0,0,0,0,"Restart");
		
		this.restartPressed = false;
		
		this.timer = new Timer();
		
		this.resize(x,y,width,height);
		
		
	}
	
	revealRestartButton(){
		if(this.width > this.height*5){
			//side by side arrangement
			this.restartButton.resize(this.x,this.y,this.width/3,this.height);
		}
		else{
			//over under arrangement.
			this.restartButton.resize(this.x,this.y,this.width,this.height/3*2);
		}
	}
	
	setWin(){
		this.state = "win";
		this.textBox.setText("You have won!");
		this.textBox.setColor("green");
		this.revealRestartButton();
		this.timer.stop();
	}
	
	ifWin(){
		return this.state==="win";
	}
	
	setLose(){
		this.state = "lose";
		this.textBox.setText("Game Over!");
		this.textBox.setColor("red");
		this.revealRestartButton();
		this.timer.stop();
	}
	
	ifLose(){
		return this.state === "lose";
	}
	
	setBombsRemaining(x){
		this.bombsRemaining = x;
		this.scoreBox.setText(x.toString());
	}
	
	getIfRestartClicked(){
		return this.restartPressed;
	}
	
	draw(context){
		if(this.obscured){
			return;
		}
		//fill the background.
		context.fillStyle = "gray";
		context.fillRect(this.x,this.y,this.width,this.height);
		
		this.scoreBox.draw(context);
		
		this.textBox.draw(context);
		
		if(this.state!="game"){//if the game is over, then draw the restart button
			this.restartButton.draw(context);
		}
		
		this.timer.draw(context);
	}
	
	onClick(x,y){
		this.restartPressed = this.restartButton.onClick(x,y);
	}
	
	resize(x,y,w,h){
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		
		//if the ui is too small, it is obscured and should not be drawn.
		if(w<0 || h<0){
			this.obscured = true;
		}
		else{
			this.obscured = false;
		}
		
		//change ui layout depending on the size.
		if(w>h*5){
			//side by side arrangement
			this.scoreBox.resize(x,y,w/3,h);
			this.textBox.resize(x+w/3,y,w/3*2,h);
			if(this.state != "game"){
				this.restartButton.resize(x,y,w/3,h);
			}
		}
		else{
			//over under arrangement.
			this.scoreBox.resize(x,y,w,h/3*2);
			this.textBox.resize(x,y+h/3*2,w,h/3);
			if(this.state!= "game"){
				this.restartButton.resize(x,y,w,h/3*2);
			}
		}
		
		this.timer.onResize(x,y,w/4,h/8);
	}
	
	onSecond(){
		this.timer.onSecond();
	}
}

class Main{
	constructor(context){
		
		//master should hold everything in it.
		this.context = context;
		
		this.menu = new Menu(context);
		
		this.b;
		
		
		this.state = "menu";
		
		this.ui = new UI(0,0,10,10);
		
		
		this.gameover = false;
	}
	
	beginGame(diff){
		//switch to the game state and build the board depending on the difficulty level selected
		this.state = "game";
		switch(diff){
			case 0:
			this.b = new Board(20,20,canvas.height-40,canvas.height-40,10,10,10);
			break;
			case 1:
			this.b = new Board(20,20,canvas.height-40,canvas.height-40,16,16,40);
			break;
			case 2:
			this.b = new Board(20,20,canvas.height-40,canvas.height-40,20,20,88);
			break;
			case 3:
			this.b = new Board(20,20,canvas.height-40,canvas.height-40,30,30,250);
			break;
			case 4:
			this.b = new Board(20,20,canvas.height-40,canvas.height-40,30,30,150);
		}
		
		this.onResize(canvas.width,canvas.height);
	}
	
	update(){
		
		
		if(this.state ==="game"){
			
			//update the ui's bombs remaining counter.
			this.ui.setBombsRemaining(this.b.getBombsRemaining());
			
			//check if the player has won
			if(this.b.getIfVictory()){
				//TODO
				//do victory stuff in here.
				this.ui.setWin();
			}//check if the player has lost.
			else if(this.b.isGameOver()){
				this.ui.setLose();
			}
			
			//draw everything.
			//blank out the backgound.
			this.context.fillStyle ="black";
			this.context.fillRect(0,0,canvas.width,canvas.height);//fills the background.
			
			//draw the board.
			this.b.draw(this.context);
			
			//draw the ui.
			this.ui.draw(this.context);
			
			
			//check the restart button.
			if(this.ui.getIfRestartClicked()){
				//reloads the page to restart.
				location.reload();
			}
		}
		else{ //menu state.
			//draw
			this.menu.draw(this.context);
			
			//switch to game state if a menu selection was made.
			if(this.menu.wasASelectionMade()){
				this.beginGame(this.menu.getSelection());
			}
		}
	}
	
	onClick(x,y,dblclick){
		if(this.state === "game"){
			//inform things that there was a click.
			this.b.onClick(x,y,dblclick);
			this.ui.onClick(x,y);
		}
		else{ //menu state.
			this.menu.onClick(x,y);
		}
		
		//update everything
		this.update();
	}
	
	onResize(w,h){
		if(this.state ==="game"){
			//resizes and replaces all the onscreen elements
			
			//find smaller coordinate.
			var widthIsSmaller = w < h;
			var smaller = Math.min(w,h);
			var larger = Math.max(w,h);
			
			var segment = 40; //divides the screen into 40 segments for scaling and spacing reasons.
			
			
			//resize the board.
			this.b.resize(w-smaller/segment*(segment-1),smaller/segment,smaller/segment*(segment-2),smaller/segment*(segment-2));
			
			//TODO
			//resize and place the ui.
			if(widthIsSmaller){
				//place the ui under the board.
				this.ui.resize(smaller/segment,smaller,smaller/segment*(segment-2), larger - smaller*(segment+1)/segment);
			}
			else{
				//place ui to the left of the board.
				this.ui.resize(smaller/segment,smaller/segment,larger-smaller*(segment+1)/segment,smaller/segment*(segment-2));
			}	
			
		}
		else{ //menu state.
			this.menu.onResize(w,h);
		}
		
		
		//update
		this.update();
	}

	onSecond(){
		if(this.state === "game"){
			this.ui.onSecond();
			this.update();
		}
		
	}
}

  //////////////////
 //event handlers//
//////////////////

//these events adjust the input so that 0,0 is the upper left of the canvas.
//values are still in pixels though.
function onClick(event){
	var rect = canvas.getBoundingClientRect();
	main.onClick(event.clientX-rect.left,event.clientY-rect.top,false);
}

function onDblClick(event){
	var rect = canvas.getBoundingClientRect();
	main.onClick(event.clientX-rect.left,event.clientY-rect.top,true);
}

function onResize(){
	//calculates and sets new screen size before passing the values on.
	canvas.width = window.innerWidth-60;
	canvas.height = window.innerHeight-180;
	main.onResize(canvas.width,canvas.height);
}

function onSecond(){
	main.onSecond();
}

  /////////////////
 //begin program//
/////////////////

//setup the canvas.
var canvas = document.getElementById('minecanvas');
var context = canvas.getContext("2d");

//build the game.
var main = new Main(context);

////setup event listeners.
//setup mouse input
document.addEventListener("click", onClick);
document.addEventListener("dblclick",onDblClick);
window.setInterval(onSecond,1000);
//screen resize.
window.addEventListener('resize', onResize,false);

//kick off the game.
onResize(); //this will capture the current screensize and update the view.

  ///////////////
 //end program//
///////////////